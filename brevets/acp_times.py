"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
        return "Invalid Control!"


    real_time = arrow.get(brevet_start_time)
    if control_dist_km <= 200:
        val = control_dist_km/34
        new_val = math.floor(val)
        time = (val - new_val)*60 
        act_time = str(new_val) + "h" + str(round(time))
        return real_time.shift(hours = new_val, minutes = round(time)).isoformat()
    elif control_dist_km <= 400:
        val = (control_dist_km - 200)/32
        new_val = math.floor(val)
        time = (val-new_val)*60
        resid = (round(time) + 53) % 60
        hrs = (round(time) + 53) // 60
        act_time = str(new_val + hrs + 5) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs + 5, minutes = resid).isoformat()
    elif control_dist_km <= 600:
        val = (control_dist_km-400)/30
        new_val = math.floor(val)
        time = (val-new_val)*60
        resid = (round(time) + 8) % 60
        hrs = (round(time) + 8) // 60
        act_time = str(new_val + hrs + 12) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs + 12, minutes = resid).isoformat()
 
    elif control_dist_km <= 1000:
        val = (control_dist_km-600)/28
        new_val = math.floor(val)
        time = (val-new_val)*60
        resid = (round(time) + 48) % 60
        hrs = (round(time) + 48) // 60
        act_time = str(new_val + hrs + 18) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs + 18, minutes = resid).isoformat()
 
    else:
        val = (control_dist_km-1000)/26
        new_val = math.floor(val)
        time = (val - new_val) * 60
        resid = (round(time) + 5) % 60
        hrs = (round(time) + 5) // 60
        act_time = str(new_val + hrs + 33) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs + 33, minutes = resid).isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
        return "Invalid Control!"
    real_time = arrow.get(brevet_start_time)
    if control_dist_km == 0:
        return real_time.shift(hours = 1).isoformat()
    if control_dist_km <= 600:
        val = control_dist_km/15
        new_val = math.floor(val)
        time = (val-new_val)*60
        resid = (round(time)) % 60
        hrs = (round(time)) // 60
        act_time = str(new_val + hrs) + "h" + str(resid)
        return real_time.shift(hours = new_val, minutes = round(time)).isoformat()
    elif control_dist_km <= 1000:
        val = (control_dist_km-600)/11.428
        new_val = math.floor(val)
        time = (val-new_val)*60
        resid = (round(time)) % 60
        hrs = (round(time)) // 60
        act_time = str(new_val + hrs + 40) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs +40, minutes = round(resid)).isoformat()
    else:
        val = (control_dist_km-1000)/26
        new_val = math.floor(val)
        time = (val - new_val) * 60
        resid = (round(time) + 5) % 60
        hrs = (round(time) + 5) // 60
        act_time = str(new_val + hrs + 33) + "h" + str(resid)
        return real_time.shift(hours = new_val + hrs + 33, minutes = round(resid)).isoformat()








def main():
    time = arrow.get("2020-03-15 00:00").isoformat()
    print(time)
    if close_time(200,600,time) == arrow.get("2020-03-15 05:53").isoformat():
        print("hello")
    else:
        print(close_time(200,600,time))



if __name__ == "__main__":
    main()

