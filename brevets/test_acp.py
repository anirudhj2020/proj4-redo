from acp_times import *


import nose


def test_open():
    time = arrow.get("2020-03-15 00:00").isoformat()
    assert open_time(200,600,time) == arrow.get("2020-03-15 05:53").isoformat()


def test_close():
    time = arrow.get("2020-03-15 00:00").isoformat()
    assert close_time(200,600,time) == arrow.get("2020-03-15 13:20").isoformat(
)

def test_invalid_open():
    time = arrow.now()
    assert open_time(500,400,time) == "Invalid Control!"


def test_invalid_close():
    time = arrow.now()
    assert close_time(1200,1000,time) == "Invalid Control!"


def test_first_close():
    time = arrow.get("2020-04-22 00:00").isoformat()
    assert close_time(0,600,time) == arrow.get("2020-04-22 01:00").isoformat()

